FROM debian

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get clean

CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
