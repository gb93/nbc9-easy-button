The Easy Peasy way to start up a container!

```
oc new-app https://gitlab.oit.duke.edu/nbc9/nbc9-easy-button.git
```
DONE.

This will build and deploy an app that runs a sleeping container based on the debian image from docker.io. 
```
# Watch build logs
oc logs -f bc/nbc9-easy-button
# when that's done shell in!
oc rsh dc/nbc9-easy-button
```

Extra credit
```
# make a secret to use for a trigger!
oc create secret generic nbc9-easy-button-trigger --from-literal=WebHookSecretKey=$(pwgen -1c 32 1 | tr -d '\n')
# make make a trigger for GitLab
oc patch bc/nbc9-easy-button -p '{"spec":{"triggers":[{"type":"GitLab","gitlab":{"secretReference":{"name":"nbc9-easy-button-trigger"}}}]}}'
```