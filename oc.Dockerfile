FROM debian:9-slim
ARG OC_VERSION=3.11.0
RUN apt-get update \
    && apt-get install -y curl tar
WORKDIR /tmp/oc_download
ADD https://github.com/openshift/origin/releases/download/v${OC_VERSION}/openshift-origin-client-tools-v${OC_VERSION}-0cbc58b-linux-64bit.tar.gz .
RUN tar -xzf openshift-origin-client-tools-v${OC_VERSION}-0cbc58b-linux-64bit.tar.gz \
    && mv openshift-origin-client-tools-v${OC_VERSION}-0cbc58b-linux-64bit/oc /usr/local/bin
RUN oc version
## Add oc tab completion
RUN oc completion bash > ~/oc_completion.sh \
    && echo "source ~/oc_completion.sh" >> ~/.bashrc
ENV SHELL=/bin/bash
